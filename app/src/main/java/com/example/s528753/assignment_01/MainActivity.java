package com.example.s528753.assignment_01;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

// This is a GDP commit
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button bt = (Button) findViewById(R.id.button1);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String result = " ";
                TextView t = (TextView) findViewById(R.id.editText1);
                String answer = t.getText().toString();
                for(int i =0;i<answer.length();i++){
                    if(answer.charAt(i) == 'o'){
                        result += 'a';
                    }
                    else if(answer.charAt(i) == 'a'){
                        result += 'o';
                    }
                    else if(answer.charAt(i) == 'O'){
                        result += 'A';
                    }
                    else if(answer.charAt(i) == 'A'){
                        result += 'O';
                    }

                    else
                        result += answer.charAt(i);


                }


                t.setText(result);
            }
        });





    }
}
